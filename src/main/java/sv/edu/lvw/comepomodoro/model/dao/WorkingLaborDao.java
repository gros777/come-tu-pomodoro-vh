package sv.edu.lvw.comepomodoro.model.dao;

import java.util.List;

import sv.edu.lvw.comepomodoro.model.domain.WorkingLabor;

/**
 * Data Access Object Interface for Working Labor
 * 
 * @author "Williams Fernandez <williams.fernandez@mh.gob.sv>"
 *
 */
public interface WorkingLaborDao extends GenericDao<WorkingLabor> {
	
	public List<WorkingLabor> getWorkingLaborByUser(Long userId);
	public WorkingLabor getWorkingLaborByName(String workingLaborName);
	public void modify(WorkingLabor workingLabor);
}
