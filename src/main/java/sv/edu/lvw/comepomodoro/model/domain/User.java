package sv.edu.lvw.comepomodoro.model.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
 * Class to manage the information of the Working Labor for User
 * 
 * @author "Victor Hernandez<victor.hernandez@mh.gob.sv>"
 *
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class User implements DomainObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560754330711107034L;

	
	private Long userId;
	private Integer version;
	private String firstName;
	private String secondName;
	private String lastName;
	private String sourName;
	private Date birthDay;
	private String stretAddress;
	private String city;//TODO must be an entity
	private String state;//TODO must be an entity
	private String country;//TODO must be an entity
	private Set<WorkingLabor> workingLabor ; 
	
	
	public User() {
		super();
	}
	@OneToMany(fetch = FetchType.LAZY, mappedBy="user")
	public Set<WorkingLabor> getWorkinLabor() {
		return workingLabor;
	}
	public void setWorkinLabor(Set<WorkingLabor> workinLabor) {
		this.workingLabor = workinLabor;
	}
	@Id
	@GeneratedValue 
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long id) {
		this.userId = id;
	}
	@Version
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSourName() {
		return sourName;
	}
	public void setSourName(String sourName) {
		this.sourName = sourName;
	}
	public Date getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}
	public String getStretAddress() {
		return stretAddress;
	}
	public void setStretAddress(String stretAddress) {
		this.stretAddress = stretAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
