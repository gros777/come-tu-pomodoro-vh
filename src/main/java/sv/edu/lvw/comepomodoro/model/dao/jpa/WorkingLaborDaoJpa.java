package sv.edu.lvw.comepomodoro.model.dao.jpa;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import sv.edu.lvw.comepomodoro.model.dao.WorkingLaborDao;
import sv.edu.lvw.comepomodoro.model.domain.WorkingLabor;

/**
 * Implementing Class of Working Labor Data Access Object
 * 
 * @author "Williams Fernandez <williams.fernandez@mh.gob.sv>"
 *
 */
public class WorkingLaborDaoJpa extends GenericDaoJpa<WorkingLabor> implements
		WorkingLaborDao {

	/**
	 * Constructor
	 */
	public WorkingLaborDaoJpa() {
		super(WorkingLabor.class);
	}

	/* (non-Javadoc)
	 * @see sv.edu.lvw.comepomodoro.model.dao.WorkingLaborDao#getWorkingLaborByUser(sv.edu.lvw.comepomodoro.model.domain.User)
	 */
	@Override
	@Transactional(readOnly = true)
	public List<WorkingLabor> getWorkingLaborByUser(Long userId) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see sv.edu.lvw.comepomodoro.model.dao.WorkingLaborDao#getWorkingLaborByName(java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public WorkingLabor getWorkingLaborByName(String workingLaborName) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see sv.edu.lvw.comepomodoro.model.dao.WorkingLaborDao#modify(sv.edu.lvw.comepomodoro.model.domain.WorkingLabor)
	 */
	@Override
	@Transactional(readOnly = true)
	public void modify(WorkingLabor workingLabor) {
		// TODO Auto-generated method stub

	}

}
