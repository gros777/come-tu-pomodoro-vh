package sv.edu.lvw.comepomodoro.model.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Tasks implements DomainObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3046556605753439641L;
	private Long id;
	private Integer version;
	private String userOwner;
	private String taskName;
	private Date initialDate;
	private Date finalDate;
	private String jobStatus; //(Not Started, In Progress, Done)
	
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Version
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public String getUserOwner() {
		return userOwner;
	}
	public void setUserOwner(String userOwner) {
		this.userOwner = userOwner;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}
	public Date getFinalDate() {
		return finalDate;
	}
	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}	
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}	
	public void copyFrom(Tasks tasks) {
		this.userOwner = tasks.getUserOwner();
		this.taskName = tasks.getTaskName();
		this.initialDate = tasks.getInitialDate();
		this.finalDate = tasks.getFinalDate();
		this.jobStatus = tasks.getJobStatus();
	}
	
}
