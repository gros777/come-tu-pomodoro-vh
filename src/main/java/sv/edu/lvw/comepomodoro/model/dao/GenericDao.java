package sv.edu.lvw.comepomodoro.model.dao;

import java.util.List;

import sv.edu.lvw.comepomodoro.model.domain.DomainObject;


public interface GenericDao<T extends DomainObject> {

	public T get(Long id);
    public List<T> getAll();
    public void save(T object);
    public void delete(T object);

}
