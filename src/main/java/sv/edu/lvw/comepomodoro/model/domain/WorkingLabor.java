package sv.edu.lvw.comepomodoro.model.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Class to manage the information of the Working Labor for User
 * 
 * @author "Williams Fernandez <williams.fernandez@mh.gob.sv>"
 *
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WorkingLabor implements DomainObject {

	private static final long serialVersionUID = 86919895622934561L;

	private Long workingId;
	/*Start Modify April 5, 2012: addin relasionships between WorkingLabor and User Autor: Victor A. Hernández*/
	private User user;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userid", nullable = false)
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	/*End Modify - Author:Victor A. Hernández*/

	private String workingLaborName;
	private Date workingStartTime;
	private Date workingEndTime;
	private Date enlapsedTime;
	private Integer version;
	
	@Id
	@GeneratedValue
	public Long getWorkingId() {
		return workingId;
	}
	
	public void setWorkingId(Long id) {
		this.workingId = id;
	}
	
	public String getWorkingLaborName() {
		return workingLaborName;
	}
	
	public void setWorkingLaborName(String workingLaborName) {
		this.workingLaborName = workingLaborName;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getWorkingStartTime() {
		return workingStartTime;
	}

	public void setWorkingStartTime(Date workingStartTime) {
		this.workingStartTime = workingStartTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getWorkingEndTime() {
		return workingEndTime;
	}

	public void setWorkingEndTime(Date workingEndTime) {
		this.workingEndTime = workingEndTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getEnlapsedTime() {
		return enlapsedTime;
	}

	public void setEnlapsedTime(Date enlapsedTime) {
		this.enlapsedTime = enlapsedTime;
	}

	@Version
	public Integer getVersion() {
		return version;
	}
	
	public void setVersion(Integer version) {
		this.version = version;
	}
}
